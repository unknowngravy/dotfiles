local keymap = vim.keymap.set
local builtin = require('telescope.builtin')
keymap('n', '<C-l>', '<C-w>l')
keymap('n', '<C-h>', '<C-w>h')
keymap('n', '<C-j>', '<C-w>j')
keymap('n', '<C-k>', 'C-w>k')
keymap('n', '<leader>ff', builtin.find_files, {}) 
keymap('n', '<leader>fb', ':NERDTreeToggle<CR>', {silent = true})

