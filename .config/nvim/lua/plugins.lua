vim.cmd [[packadd packer.nvim]]
return require("packer").startup(function(use)
use 'wbthomason/packer.nvim'
use {
  'nvim-lualine/lualine.nvim',
  requires = { 'nvim-tree/nvim-web-devicons', opt = true }
}
use 'shaunsingh/nord.nvim'
use 'nvim-tree/nvim-web-devicons'
use {
  'nvim-telescope/telescope.nvim', tag = '0.1.1',
-- or                            , branch = '0.1.x',
  requires = { {'nvim-lua/plenary.nvim'} 
  }
}
use 'preservim/nerdtree'
use 'ryanoasis/vim-devicons'
end)

