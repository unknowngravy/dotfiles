(when (file-readable-p "~/dotfiles/.emacs.d/config.org")
  (org-babel-load-file (expand-file-name "~/dotfiles/.emacs.d/config.org")))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("2f8eadc12bf60b581674a41ddc319a40ed373dd4a7c577933acaff15d2bf7cc6" "5d1ef40b7ae10bd04f17eb98e6b7a1832e254948e4da4abe9a0ee290a9b676e0" "944d52450c57b7cbba08f9b3d08095eb7a5541b0ecfb3a0a9ecd4a18f3c28948" "aec7b55f2a13307a55517fdf08438863d694550565dee23181d2ebd973ebd6b8" "636b135e4b7c86ac41375da39ade929e2bd6439de8901f53f88fde7dd5ac3561" "adaf421037f4ae6725aa9f5654a2ed49e2cd2765f71e19a7d26a454491b486eb" "e3daa8f18440301f3e54f2093fe15f4fe951986a8628e98dcd781efbec7a46f2" "60ada0ff6b91687f1a04cc17ad04119e59a7542644c7c59fc135909499400ab8" "443e2c3c4dd44510f0ea8247b438e834188dc1c6fb80785d83ad3628eadf9294" "f74e8d46790f3e07fbb4a2c5dafe2ade0d8f5abc9c203cd1c29c7d5110a85230" "1a1ac598737d0fcdc4dfab3af3d6f46ab2d5048b8e72bc22f50271fd6d393a00" "7ea883b13485f175d3075c72fceab701b5bf76b2076f024da50dff4107d0db25" "2dc03dfb67fbcb7d9c487522c29b7582da20766c9998aaad5e5b63b5c27eec3f" "f06052bb1b4f2a6f64b18aad03b1f6851c8d587147a363e02cf45de890fe280c" "c25d00b2b71ebd0133d4512ad6070342dd0b662d30106bbacced82a6c52ade8c" "9724dd370de9086cc2ab6b0c6a563d6b4967d0262187fd6d712e8ce413eea7cd" "c225c398e56577803696fcfaa1d157e6781515d24b6702ad81d48c23e27383cf" "be8b0984e738655bf130e6b0d23dcdb70253bd2a85247fa37bd7e4252bf96c97" "290791cf0b01dab130b10376442ef07b89d1c9e4ff6a87cc07c7d8917ee2f628" "c282a528137220d5f71c84ca68eb8bd87b3ccb3656434b20ad600a380f9f198c" "bffa9739ce0752a37d9b1eee78fc00ba159748f50dc328af4be661484848e476" default))
 '(package-selected-packages
   '(pyvenv toml-mode fish-mode modus-themes tree-sitter-langs tree-sitter company-box toc-org company neotree zenburn-theme lsp-mode lsp-ui lsp-pyright treemacs-all-the-icons spacemacs-theme projectile which-key vertico use-package rainbow-mode org-bullets magit jedi general evil doom-themes doom-modeline dashboard beacon all-the-icons-dired))
 '(warning-suppress-types
   '((emacs)
     (emacs)
     (emacs)
     (use-package)
     (with-editor)
     (use-package)
     (use-package)
     (use-package)
     (use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-level-1 ((t (:inherit outline-1 :height 1.7))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.5))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.3)))))
