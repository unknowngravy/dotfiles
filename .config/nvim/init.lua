require("plugins")
require('settings')
require("keybind")

-- LUALINE
require('lualine').setup {
  options = {
    -- ... your lualine config
    theme = 'nord'
    -- ... your lualine config
  }
}

--Nord
vim.cmd[[colorscheme nord]]

