local o = vim.o
local g = vim.g


o.relativenumber = true
o.tabstop = 4
o.softtabstop = 4
o.shiftwidth = 4
o.scrolloff = 8
o.expandtab = true
o.smartindent = true
o.swapfile = false
o.writebackup = false
g.mapleader = " "

